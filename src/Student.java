package com.apple.impactanalysis;
/**
 * 
 * @author Test
 *
 */
public class Student {
    private long id;

    private String firstName;

    public Student(long id, String firstName, String lastName, String gender,
            int age) {
        super();
        this.id = id;
        this.firstName = firstName;
        

    }

    public long getId() {
        return id;
    }


    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;

    }
}