package com.apple.impactanalysis;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import org.json.JSONException;
import org.json.JSONObject;

public class BitbucketConnect 
{

	public static void main(String[] args) throws MalformedURLException, IOException, JSONException {
		// TODO Auto-generated method stub
		ArrayList<String> lines = new ArrayList<>();
		String username = "Sre0227";
		String apppassword = "Apple@123";
		String reponame = "appletest";
		byte[] encodedAuth = Base64.getEncoder().encode((username+":"+apppassword).getBytes());
		//Added comments and commited
		HttpURLConnection connection = (HttpURLConnection) new URL("https://api.bitbucket.org/2.0/repositories/"+username+"/"+reponame+"/commits").openConnection();
		connection.setRequestProperty("Authorization", "Basic " + new String(encodedAuth));
		BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		System.out.println(reader.readLine());
		JSONObject lastestCommit = new JSONObject(new JSONObject(reader.readLine()).getJSONArray("values").get(0).toString());
		connection = (HttpURLConnection) new URL("https://api.bitbucket.org/2.0/repositories/"+username+"/"+reponame+"/src/"+lastestCommit.getString("hash")+"/names?raw").openConnection();
			connection.setRequestProperty("Authorization", "Basic " + new String(encodedAuth));
		reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String line;
		while ((line = reader.readLine()) != null) {
		    lines.add(line);
		}
		connection.disconnect();
	}

}
